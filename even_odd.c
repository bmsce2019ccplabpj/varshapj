#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number:\n");
    scanf("%d",&n);
    return n;
} 
int compute(int n) 
{
    int result;
    if(n%2==0) 
    {
        result=0;
    }
    else
    {
        result=1;
    }
    return result;
}
void display(int n,int result) 
{
    if(result==0) 
    {
        printf("%d is even\n",n);
    }
    else
    {
        printf("%d is odd\n",n);
    }
}
int main() 
{
    int n,result;
    n=input();
    result=compute(n);
    display(n,result);
    return 0;
}