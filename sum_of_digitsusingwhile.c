#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number:\n");
    scanf("%d",&n);
    return n;
}
int compute(int num)
{
    int sum=0;
    while(num>0)
    {
        int rem=0;
        rem=num%10;
        num=num/10;
        sum=sum+rem;
    }
    return sum;
}
void display(int sum_of_digits,int num)
{ 
    printf("Sum of digits of %d=%d",num,sum_of_digits);
}
int main()
{
    int number, total;
    number=input();
    total=compute(number);
    display(total,number);
    return 0;
}
    
 
        