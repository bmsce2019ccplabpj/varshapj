#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    float r1,r2;
    printf("Enter the coefficients:\n");
    scanf("%d%d%d",&a,&b,&c);
    int discriminant;
    discriminant=b*b-(4*a*c);
    if(discriminant==0)
    {
        printf("The roots are reaL and equal\n");
        r1=(-b+sqrt(discriminant))/(2*a);
        r2=r1;
    }
    else if(discriminant>0)
    {  
        printf("The roots are real and unequal\n");
        r1=(-b+sqrt(discriminant))/(2*a);
        r2=(-b-sqrt(discriminant))/(2*a);
    }
    else if(discriminant<0)
    {
        printf("The roots are imaginary\n");
    }
    printf("The roots of the quadratic equation=%f and %f",r1,r2);
    return 0;
}
