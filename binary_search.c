#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number of elements in the array:\n");
    scanf("%d",&n);
    return n;
}
int compute(int n,int key)
{
    int a[n];
    printf("Enter the elements of array in ascending order:\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    int beg=0,mid,flag;
    int end=n-1,pos;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==key)
        {
            flag=1;
            break;
        }
        else if(a[mid]<key)
        {
            beg=mid+1;
        }
        else
        {
            end=mid-1;
        }
    }
    if(flag==1)
    {
        pos=mid;
    }
    else
    {
        pos=-5;
    }
    return (pos+1);
}
void display(int pos,int key)
{
    if(pos>0)
    printf("The element %d is found in position %d\n",key,pos);
    else
    printf("Invalid input\n");
}
int main()
{
    int n,pos,key;
    printf("Enter the number to be searched:\n");
    scanf("%d",&key);
    n=input();
    pos=compute(n,key);
    display(pos,key);
    return 0;
}
