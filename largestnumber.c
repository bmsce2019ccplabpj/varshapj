#include<stdio.h>
int input()
{
    int num;
    printf("Enter the value of number:\n");
    scanf("%d", &num);
    return num;
}
int compute(int n1,int n2, int n3)
{
    int result;
    if(n1>n2&&n1>n3)
    {
        result=n1;
    }
    else if(n2>n3)
    {
        result=n2;
    }
    else
    {
        result=n3;
    }
    return result;
}
void output(int result,int n1,int n2,int n3)
{
    printf("Largest of the 3 numbers %d, %d, %d = %d",n1,n2,n3,result);
}
int main()
{
    int num1,num2,num3;
    num1=input();
    num2=input();
    num3=input();
    int result=compute(num1,num2,num3);
    output(result,num1,num2,num3);
    return 0;
    
}
