#include<stdio.h>
int input()
{
    int num;
    printf("Enter the number:\n");
    scanf("%d",&num);
    return num;
}
int compute(int a)
{
    int n=a;
    int result=0;
    while(a!=0)
    { 
        int rem=0;
        rem=a%10;
        a=a/10;
        result=10*result+rem;
    }
    int x;
    if(result==n)
    x=1;
    else 
    x=0;
    return x;
}
void display(int c)
{
    if(c==1)
    {
         printf("It is a palindrome number\n");
    }
    else
    {
         printf("It is not a palindrome number\n");
    }
}
int main()
{
    int number, result;
    number=input();
    result=compute(number);
    display(result);
    return 0;
}
