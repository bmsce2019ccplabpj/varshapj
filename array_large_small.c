#include<stdio.h>
int main()
{
    int n=0;
    printf("Enter the number of digits to be entered:\n");
    scanf("%d",&n);
    int a[n];
    for(int i=0;i<n;i++)
    {
        printf("Enter the number:\n");
        scanf("%d",&a[i]);
    }
    int large=0,small=0;
    int l=0,s=0;
    for(int j=0;j<n;j++)
    {  
        if(a[j]>large)
        {
            large=a[j];
            l=j;
        }
        if(a[j]<small)
        {
            small=a[j];
            s=j;
        }
    }
    printf("The largest number is %d found in position %d\n",a[l],(l+1));
    printf("The smallest number is %d found in position %d\n",a[s],(s+1));
    int temp=0;
    temp=a[l];
    a[l]=a[s];
    a[s]=temp;
    printf("The altered array is :\n");
    for(int k=0;k<n;k++)
    {
         printf("%d\n",a[k]);
    }    
    return 0;
}

