#include<stdio.h>
int hours() 
{
   int hr;
   printf("Enter the time in hrs:\n");
   scanf("%d",&hr);
   return hr;
}
int minutes() 
{
   int min;
   printf("Enter the time in minutes:\n");
   scanf("%d",&min);
   return min;
}
int compute(int hr,int min) 
{
   int total_min;
   total_min=(hr*60)+min;
   return total_min;
}
void display(int hr,int min,int total_min) 
{
   printf("%d hours %d minutes is converted to %d minutes\n",hr,min,total_min);
}
int main() 
{
   int hr,min,total_min;
   hr=hours();
   min=minutes();
   total_min=compute(hr,min);
   display(hr,min,total_min);
   return 0;
}