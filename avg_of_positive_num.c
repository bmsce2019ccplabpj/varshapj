#include<stdio.h>
int input() 
{
   int n;
   printf("Enter the number of digits to be entered:\n");
   scanf("%d",&n);
   return n;
}
float compute(int n) 
{
   int sum=0;
   int count=0;
   float avg;
   for(int i=1;i<=n;i++)
   {
      int number;
      printf("Enter the number:\n");
      scanf("%d",&number);
      if(number>=0)
      {
         sum=sum+number;
         count++;
      }
   }
   avg=sum/count;
   return avg;
}
void display(float avg) 
{
   printf("Average of positive numbers=%f",avg);
}
int main() 
{
   int n;
   float avg;
   n=input();
   avg=compute(n);
   display(avg);
   return 0;
}