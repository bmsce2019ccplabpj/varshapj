#include<stdio.h>
void input(int a[5][3])
{
    for(int i=0;i<5;i++) 
    {
        printf("Enter the marks of student %d:\n",(i+1));
        for(int j=0;j<3;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
} 
void compute(int a[5][3],int high_marks[3]) 
{
    int i,j=0;
    for(j=0;j<3;j++)
    {
        high_marks[j]=a[i][j];
        for(i=0;i<5;i++)
        {
            if(a[i][j]>high_marks[j]) 
            {
                high_marks[j]=a[i][j];
            }
        }
    }
}
void display(int high_marks[3]) 
{
    for(int i=0;i<3;i++) 
    {
         printf("Highest marks in course %d is %d\n",(i+1),high_marks[i]);
    }
}
int main() 
{
    int a[5][3];
    int high_marks[3];
    input(a);
    compute(a,high_marks); 
    display(high_marks);
    return 0;
}