#include<stdio.h>
#include<conio.h>
struct Employee
{
   int ID;
   char name[20];
   float salary;
   char DOJ[10];
};
void number_of_Employees(int *n) 
{
   printf("Enter the number of employees:\n");
   scanf("%d",n);
}
void input(int n,struct Employee E[n]) 
{
   for(int i=0;i<n;i++)
   {
       printf("Enter ID of %d employee:\n",(i+1));
       scanf("%d",&E[i].ID);
       printf("Enter name of %d employee:\n",(i+1));
       scanf("%s",&E[i].name);
       printf("Enter salary of %d employee:\n",(i+1));
       scanf("%f",&E[i].salary);
       printf("Enter date of join of %d employee:\n",(i+1));
       scanf("%s",&E[i].DOJ);
   }
}
void output(int n,struct Employee E[n]) 
{
   for(int i=0;i<n;i++)
   {
       printf("Details of Employee %d\n",(i+1));
       printf("Employee ID = %d\n",E[i].ID);
       printf("Employee name = %s\n",E[i].name);
       printf("Salary of the Employee = %f\n",E[i].salary);
       printf("Date of join of the Employee = %s\n",E[i].DOJ);
   }
}
int main() 
{
   int n;
   number_of_Employees(&n);
   struct Employee E[n];
   input (n,E);
   output(n,E);
   return 0;
}