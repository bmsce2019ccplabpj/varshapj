#include<stdio.h>
int input()
{
    int num;
    printf("Enter the number:\n");
    scanf("%d",&num);
    return num;
}
int compute(int a)
{
    int result=0;
    while(a!=0)
    { 
        int rem=0;
        rem=a%10;
        a=a/10;
        result=10*result+rem;
    }
    return result;
}
void display(int rev)
{
     printf("Reversed number=%d\n",rev);
}
int main()
{
    int number, result;
    number=input();
    result=compute(number);
    display(result);
    return 0;
}
