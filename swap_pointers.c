#include<stdio.h>
void input(int *a,int *b) 
{
    printf("Enter 1st number:\n");
    scanf("%d",a);
    printf("Enter 2nd number:\n");
    scanf("%d",b);
}
void compute(int *a,int *b) 
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
void display(int a,int b) 
{
    printf("a=%d\n",a);
    printf("b=%d\n",b);
}
int main() 
{
    int a,b;
    input(&a,&b);
    compute(&a,&b);
    display(a,b);
    return 0;
}