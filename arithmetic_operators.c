#include <stdio.h>
int main() 
{ 
    int a, b, sum, diff, pro, rem;
    float quo; 
    printf("Enter 2 numbers:\n");
    scanf("%d%d",&a,&b); 
    sum=a+b; 
    diff=a-b; 
    pro=a*b;
    quo=a/b; 
    rem=a%b; 
    printf("%d+%d=%d\n",a,b,sum);
    printf("%d-%d=%d\n",a,b,diff);
    printf("%d*%d=%d\n",a,b,pro);
    printf("%d/%d=%f\n",a,b,quo);
    printf("%d%%d=%d\n",a,b,rem);
    return 0;
}